require('dotenv').config()
const { Telegraf } = require('telegraf')
const token = process.env.TELEGRAM_BOT_TOKEN
const chat_id = process.env.TELEGRAM_OVERLORD_CHAT_ID="365874331"
const bot = new Telegraf(token)

const requests = require('./requests.js');

const hiveTx = require('hive-tx');

function sleep(ms) {
    return new Promise(resolve=>setTimeout(resolve, ms))
}

async function createCustomJSON(player, key, id, data, active = false) {
    let privateKey = hiveTx.PrivateKey.from(key)
    let operations = [[
        "custom_json",
        {
            "required_auths": active ? [player] : [],
            "required_posting_auths": active ? [] : [player],
            "id": id,
            "json": JSON.stringify(data)
        }
    ]];

    const tx = new hiveTx.Transaction()
    await tx.create(operations).then(() => tx.sign(privateKey))

    let encoded = new URLSearchParams();
    encoded.append('signed_tx', JSON.stringify(tx.signedTransaction));

    return encoded.toString();
}

async function harvestDEC(player, activeKey, balance) {
    let data = { to: 'evanstinger', qty: balance, token: 'DEC', type: 'withdraw', memo: 'evanstinger' };
    let tx = await createCustomJSON(player, activeKey, "sm_token_transfer", data, true)
    
    let result = await requests.postBroadcastSend(tx);
    console.log(result);
}

async function harvestCards(player, activeKey, cards) {
    let data = { to: 'evanstinger', cards: cards};
    let tx = await createCustomJSON(player, activeKey, "sm_gift_cards", data, true)
    
    let result = await requests.postBroadcastSend(tx);
    console.log(result);
}

let crops;

(async () => {

    console.log('Start harvesting')
    await bot.telegram.sendMessage(chat_id, 'Start daily bot harvesting on: ' + new Date().toLocaleString())
    crops = await requests.initHarvest();
    
    console.log('Crops retrieved');

    let totalDEC = 0;

    // Checking and harvesting DEC
    for (let i = 0; i < crops.length; i++) {
        console.log('Checking for DEC:', crops[i].username)
        let dec = await requests.getDECbalance(crops[i].username)
        if (parseInt(dec) > 10) {
            console.log('Harvesting', dec, 'DEC', crops[i].username)
            await harvestDEC(crops[i].username, crops[i].key, dec)
            console.log(dec, 'DEC harvested')
            totalDEC += dec;
        }
    }

    // Checking and harvesting reward cards
    for (let i = 0; i < crops.length; i++) {
        console.log('Checking for cards:', crops[i].username)
        const owned = await requests.getOwnedCards(crops[i].username);
        if (owned.length > 0) {
            console.log('Harvesting', owned.length, 'cards from', crops[i].username)
            await harvestCards(crops[i].username, crops[i].key, owned)
            
            let cardDetails = await requests.findCardbyID(owned.toString());
            let photos = [];
            for (let i = 0; i < cardDetails.length; i++) {
                let img = encodeURI(cardDetails[i].details.name)
                if (cardDetails[i].gold) {
                    img.concat('_gold')
                }
                let photo = {type: 'photo', media: `https://d36mxiodymuqjm.cloudfront.net/cards_beta/${img}.jpg`}
                photos.push(photo)
            }

            await bot.telegram.sendMessage(chat_id, 'Cards harvested from ' + crops[i].username)
            await bot.telegram.sendMediaGroup(chat_id, photos)

        }
    }

    console.log('done, harvested', totalDEC, 'DEC')
    await bot.telegram.sendMessage(chat_id, 'Total DEC Harvested:' + totalDEC + ' DEC')
})();