//'use strict';
require('dotenv').config()
const puppeteer = require('puppeteer');

const splinterlandsPage = require('./splinterlandsPage');
const user = require('./user');
const card = require('./cards');
const { clickOnElement, getElementText, getElementTextByXpath, teamActualSplinterToPlay } = require('./helper');
const quests = require('./quests');
const ask = require('./possibleTeams');

const sleepingTime = process.env.BATTLES_INTERVAL_IN_SECONDS || 3 * 1000;
const isHeadlessMode = process.env.HEADLESS === 'false' ? false : true; 

// current Q
let q = parseInt(process.env.STARTING_Q) || 1;

// Bot mode objectives, 1 = DQ, 2 = ECR >75, 3 ECR
let mode = 1;

// Main Loop
(async () => {
    while (true) {
        while (q != undefined) {
            // Start new session and login:
            let player = process.env['ACCOUNT_' + q];
            let password = process.env['PASSWORD_' + q];

            if (player == undefined) {
                console.log('Routine Finished');
                process.exit();
            }
            
            console.log('START ', player, new Date().toLocaleString())
    
            const browser = await puppeteer.launch({
                headless: isHeadlessMode,
                args: ['--no-sandbox']
            }); 
            
            const page = await browser.newPage();
            await page.setDefaultNavigationTimeout(500000);
            await page.setUserAgent('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36');
            await page.setViewport({
                width: 1200,
                height: 800,
                deviceScaleFactor: 1,
            });

            // Accepting any confirm() alert dialog
            await page.on('dialog', async dialog => {
                console.log(dialog.message());
                await dialog.accept();
            });

            // Intercept Image Request to make Puppeteer lighter
            await page.setRequestInterception(true);
            page.on('request', (req) => {
                if (req.resourceType() === 'image') {
                req.abort();
                } else {
                req.continue();
                }
            });
    
            await page.goto('https://splinterlands.io/', { waitUntil: ['networkidle2'] });

            // Prepare cards
            console.log('getting user cards collection from splinterlands API...')
            const myCards = await getCards(player)
                .then((x)=>{console.log('cards collection ready!'); return x})
                .catch(()=> {console.log('cards collection api didnt respond.'); throw new Error('get Cards Error')});

            // Prepare to login
            let login_btn = await page.waitForSelector('#log_in_button > button', {
                visible: true,
            })
            .then(res => res)
            .catch(()=> console.log('Already logged in'))
    
            if (login_btn != undefined) {
                console.log('Login attempt...');
                const loginData = await splinterlandsPage.login(page, player, password)
                    .then((x)=>{console.log('Login Success'); return x})
                    .catch(()=> {console.log('Login Failed.'); throw new Error('Login Failed')});
                
                console.log(player, 'Rating:', loginData.rating, ' ECR:', loginData.capture_rate/100, '% CP:', loginData.collection_power, ' DEC:', loginData.dec_balance);
            }
            
            await closePopups(page);

            await page.goto('https://splinterlands.com/?p=battle_history', { waitUntil: ['networkidle0'] });
            await closePopups(page);
            
            // Quest counter
            let left = 1;

            console.log(player, 'getting quest info from splinterlands API...')
            let quest = await getQuest(player);
            if(quest) {
                // Avoid Sneak and Snipe quest to have faster completion on daily quests
                const avoid = ['sneak', 'snipe'];

                if (quest.refresh == null && quest.claim == null && avoid.some(v => quest.splinter.includes(v))) {
                    try {
                        await page.$eval( 'button#quest_new_btn', button => button.click() )
                        console.log(player, 'atempt to get easier quest')
                        await page.waitForTimeout(3000)
                        quest = await getQuest(player)
                        console.log('New quest details:', quest)
                    } catch (e) {
                        console.log('Fail when atempted to refresh daily quest')
                        throw new Error()
                    }
                    
                }
                
            } else {
                console.log('Error for quest details.')
            }
    
            // The auto battling loop
            while (left) {

                try {
                    await page.goto('https://splinterlands.com/?p=battle_history');
                    await page.waitForTimeout(5000);
                    console.log(player, 'Doing Ranked Match Loop', new Date().toLocaleString())

                    await rankedMatchBotLoop(page, myCards, quest, player)
                        .then(() => {
                            console.log(player, 'Closing battle', new Date().toLocaleString());        
                        })
                        .catch((e) => {
                            console.log(e, 'Error on RankedMatchBotLoop Catch')
                        })
                    //Check if quest completed 
                    quest = await getQuest(player);
                    console.log('Quest details: ', quest);
                    left = quest.total - quest.completed;
                    // claim quest reward when DQ completed
                    if (!left) {
                        try {
                            await page.waitForSelector('#quest_claim_btn', { timeout: 5000 })
                                .then(button => button.click());
                                console.log('DQ reward claimed.');
                                // check if there are stacked DQs
                                quest = await getQuest(player);
                                if (quest) {
                                    left = quest.total - quest.completed;
                                }
                                if (quest && left) {
                                    console.log('New Quest details: ', quest);
                                }
                        } catch (e) {
                            console.info('no quest reward to be claimed waiting for the battle...')
                        }
                        
                    }
                    
                } catch (e) {
                    console.log('Routine error at: ', new Date().toLocaleString(), e)
                }
                await console.log(player,' waiting for the next battle in', sleepingTime, 'seconds' )
                await new Promise(r => setTimeout(r, sleepingTime));
            }
            // increment q to move to next accounts
            q++;
            await browser.close();
            console.log('Account change.')
        }
        
    }
})();

// functions
async function getCards(player) {
    const myCards = await user.getPlayerCards(player);
    return myCards;
} 

async function getQuest(player) {
    return quests.getPlayerQuest(player)
        .then(x=>x)
        .catch(e=>console.log('No quest data, splinterlands API didnt respond'))
}

async function closePopups(page) {
    console.log('check if any modal needs to be closed...')
	if (await clickOnElement(page, '.close', 4000) ) return;
	await clickOnElement(page, '.modal-close-new', 1000, 2000);
    await clickOnElement(page, '.modal-close', 4000, 2000);
}


async function rankedMatchBotLoop(page, myCards, quest, player) {

    try {        
        console.log('waiting for BATTLE button...')
        await page.waitForXPath('//*[@id="battle_category_btn"]', { timeout: 10000 })
            .then(button => {console.log('Battle button clicked'); button.click()[0]})
            .catch(e=>{console.error('[ERROR] waiting for BATTLE button. maintenance?'); throw new Error()});
        
        await page.waitForTimeout(5000);
        
        console.log('waiting for Create Team button...')

        await page.waitForSelector('.btn--create-team', { timeout: 10000 })
            .then(()=>console.log('Create Team button found, now prepare team...'))
            .catch(async (e)=> {
                console.log('Create Team Button not visible', e);
                throw new Error();
            })

    } catch(e) {
        throw new Error(e, 'Error on Click Battle and Wait for Create Team buttons');
    }

    await page.waitForTimeout(5000);

    // Get Mana, Rules and Active Splinters from Create Team Dialogue Box
    let [mana, rules, splinters] = await Promise.all([
        splinterlandsPage.checkMatchMana(page).then((mana) => mana).catch(() => 'no mana'),
        splinterlandsPage.checkMatchRules(page).then((rulesArray) => rulesArray).catch(() => 'no rules'),
        splinterlandsPage.checkMatchActiveSplinters(page).then((splinters) => splinters).catch(() => 'no splinters')
    ]);

    const matchDetails = {
        mana: mana,
        rules: rules,
        splinters: splinters,
        myCards: myCards
    }
    // Pass the Match Details to Possible Teams script to find best possible team for match details
    const possibleTeams = await ask.possibleTeams(matchDetails).catch(e=>console.log('Error from possible team: ',e));

    if (possibleTeams && possibleTeams.length) {
        console.log('Possible Teams based on your cards: ', possibleTeams.length);
    } else {
        console.log('Error:', matchDetails, possibleTeams)
        throw new Error('NO TEAMS available to be played');
    }
    
    //Run the algorithm for team selection based on agregated history data against match rulesets
    const teamToPlay = await ask.teamSelection(possibleTeams, matchDetails, quest);
    if (teamToPlay) {
        console.log('Team selected.')
    } else {
        throw new Error('Team Selection error');
    }

    //Select the team and submit within this loop to prevent stuck on error and hanging process
    let retries = 0;
    while (retries < 7) {
        try {
            if (retries > 0) {
                await page.reload();
                await page.waitForTimeout(3000);
            }

            // Click Create Team on pending battle modal
            try {
                await page.click('.btn--create-team')[0];;
                console.log('Create Team Clicked')
                await page.waitForTimeout(5000);
            } catch (error) {
                console.log('Create Team Failed');
                throw new Error();
            }
            
            // Select Summonner
            await page.waitForXPath(`//div[@card_detail_id="${teamToPlay.summoner}"]`, { timeout: 10000 })
                .then(summoner => summoner.click())
                .catch(()=>{
                    console.log('Failed to click summoner, retrying')
                    throw new Error();  
                });

            // Select companion if using Dragon Summoner
            if (card.color(teamToPlay.cards[0]) === 'Gold') {
                console.log('Dragon play TEAMCOLOR', teamActualSplinterToPlay(teamToPlay.cards.slice(0, 6)))
                await page.waitForXPath(`//div[@data-original-title="${teamActualSplinterToPlay(teamToPlay.cards.slice(0, 6))}"]`, { timeout: 8000 })
                    .then(selector => selector.click())
                    .catch( ()=> {
                        console.log('Dragon\'s companion divId not found, retrying...'); 
                        throw new Error();
                    });
            }

            await page.waitForTimeout(3000);

            // Select Monsters
            try {
                for (i = 1; i <= 6; i++) {
                    await teamToPlay.cards[i] ? page.waitForXPath(`//div[@card_detail_id="${teamToPlay.cards[i].toString()}"]`, { timeout: 10000 })
                        .then(selector => {selector.click(); console.log('play: ', teamToPlay.cards[i].toString())}).catch( ()=> {
                            console.log('fail to select monster cards no:', i)
                            throw new Error(); 
                        }) : console.log('empty');
                    await page.waitForTimeout(1000);
                }
            } catch (error) {
                console.log('fail to select monster cards');
                throw new Error();
            }
    
            await page.waitForTimeout(3000);

            try {
                await page.$eval("#page_container > div > div.deck-builder-page2__deck > div > button", el => el.click()); //start fight
                console.log('Team submitted, now waiting for Rumble')
                break;
            } catch {
                console.log('Fail clicking fight button'); 
                throw new Error(); 
            }
        } catch (e) {
            console.log(e, 'error on submit cards loop')
            retries++;
            continue;
        }
        

    }

    if (retries >= 6) {
        console.log('Too many retries, restarting...');
        throw new Error();
    }
 
    // Battle animation and result
    try {
        
        await page.waitForTimeout(5000);
        await page.waitForSelector('#btnRumble', {
            timeout: 90000,
            visible: true,
          })
            .then(()=>console.log('btnRumble visible'))
            .catch(async (e)=> { 
                await browser.close();
                console.log(e, ' error watching result, ignore it, just restarting...');
                return;
            });
        await page.$eval('#btnRumble', elem => elem.click()).then(()=>console.log('btnRumble clicked')).catch(()=>console.log('btnRumble didnt click')); //start rumble
        await page.waitForSelector('#btnSkip', { timeout: 10000 }).then(()=>console.log('btnSkip visible')).catch(()=>console.log('btnSkip not visible'));
        await page.$eval('#btnSkip', elem => elem.click()).then(()=>console.log('btnSkip clicked')).catch(()=>console.log('btnSkip not visible')); //skip rumble
        await page.waitForTimeout(10000);
        try {
			const winner = await getElementText(page, 'section.player.winner .bio__name__display', 15000);
			if (winner.trim() == player) {
				const decWon = await getElementText(page, '.player.winner span.dec-reward span', 1000);
				console.log(player, ' won! Reward: ' + decWon + ' DEC');
			}
			else {
                console.log('Battle lost');
			}
		} catch {
			console.log('Could not find winner - draw?');
		}
		await clickOnElement(page, '.btn--done', 20000, 10000);
    } catch (e) {
        console.log(e, ' error watching result, ignore it, just restarting...');
        return;
    }


}

