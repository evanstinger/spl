require('dotenv').config()
const md5 = require('md5')
const { generatePassword } = require('./helper');
const quests = require('./quests');
const ask = require('./possibleTeams');
const requests = require('./requests.js');

const hiveTx = require('hive-tx');

const sleepingTime = process.env.BATTLES_INTERVAL_IN_SECONDS * 1000 || 3 * 1000;

const verbose = process.env.VERBOSE || false;
let running = false;
let player;
let _postingKey;
let postingKey
let details;
let team;
let myCards;
let lastReveal;
let credentials;
let quest = {};

process.on('uncaughtException', function(e) {
    console.log('An error has occured. error is: %s and stack trace is: %s', e, e.stack);
    console.log("Process will restart now.");
    process.exit(1);
})

// functions
function sleep(ms) {
    return new Promise(resolve=>setTimeout(resolve, ms))
}

async function getQuest(player) {
    return quests.getPlayerQuest(player)
        .then(x=>x)
        .catch(e=>console.log('No quest data, splinterlands API didnt respond'))
}

async function createCustomJSON(player, key, id, data, active = false) {
    let operations = [[
        "custom_json",
        {
            "required_auths": active ? [player] : [],
            "required_posting_auths": active ? [] : [player],
            "id": id,
            "json": JSON.stringify(data)
        }
    ]];

    const tx = new hiveTx.Transaction()
    await tx.create(operations).then(() => tx.sign(key))

    let encoded = new URLSearchParams();
    encoded.append('signed_tx', JSON.stringify(tx.signedTransaction));

    return encoded.toString();
}

async function findMatch(player, postingKey) {
    let data = { match_type: 'Ranked' };
    let tx = await createCustomJSON(player, postingKey, "sm_find_match", data);
    
    let result = await requests.postBattleTx(tx);
    return result;
}

async function submitTeam(trx_id, myCards, teamToPlay, player, postingKey) {
    console.log('start submit team')
    let summoner = myCards.filter(x => parseInt(x.card_detail_id) == parseInt(teamToPlay.summoner));
    if (summoner.length > 1) {
        // Get the highest level card available
        summoner = summoner.reduce((prev, current) => (+prev.level > +current.level) ? prev : current)
    } else {
        summoner = summoner[0]
    }
    
    let summonerUid = summoner.uid;
    
    if (summonerUid.startsWith('starter')) {
        summonerUid = summonerUid.concat(generatePassword(5));
    }
    
    let monsters = [];
    let cardUid;
    for (i = 1; i <= 6; i++) {
        
        if (teamToPlay.cards[i] !== '' && teamToPlay.cards[i] !== undefined) {
            let card = myCards.filter(x => parseInt(x.card_detail_id) == parseInt(teamToPlay.cards[i]));
            
            if (card.length > 1) {
                // Get the highest level card available
                card = card.reduce((prev, current) => (+prev.level > +current.level) ? prev : current)
            } else {
                card = card[0]
            }
            cardUid = card.uid;
            if (cardUid.startsWith('starter')) {
                cardUid = cardUid.concat(generatePassword(5));
            }
            monsters.push(cardUid);
        }
        
    }
    
    const secret = generatePassword(10);
    const team_hash = md5(summonerUid + "," + monsters.join() + "," + secret);

    // save team to variable
    team = {
        summoner: summonerUid,
        monsters: monsters,
        secret: secret
    };

    // verbose ? console.log(team) : '';
    
    const data = {
        trx_id: trx_id,
        team_hash: team_hash
    };

    let tx = await createCustomJSON(player, postingKey, "sm_submit_team", data);
    
    let result = await requests.postBattleTx(tx);
    return result;
}

async function checkBattleStatus(queue_trx) {
    
    let result;

    try {
        result = await requests.getBattleStatus(queue_trx);
    } catch (error) {
        console.log('Error checking Battle Status', error)
        return false;
    }

    if (!result || result.status >= 2 || result.reveal_tx) {
        console.log('Battle has ended')
        return true;
    }
            
    if (result.team_hash && result.opponent_team_hash) {
        console.log('Opponnent submitted, now reveal');
        return true;
        
    } else {
        console.log('Waiting for opponent submitting their team...');
        return false;
    }


}

async function revealTeam(queue_trx, team, player, postingKey) {
    lastReveal = queue_trx;
    const data = {
        trx_id: queue_trx,
        summoner: team.summoner,
        monsters: team.monsters,
        secret: team.secret
    };

    let tx = await createCustomJSON(player, postingKey, "sm_team_reveal", data);
    
    let result = await requests.postBattleTx(tx);
    return result;
}

async function claimDailyQuestReward(player, postingKey, quest_id) {
    let data = {type: "quest", quest_id: quest_id};
    let tx = await createCustomJSON(player, postingKey, "sm_claim_reward", data);
    
    await requests.postBroadcastSend(tx)
        .then((x)=>console.log(x))
        .catch(()=> console.log('Failed send broadcast claimDailyQuestReward .'));
    
}

async function refreshDailyQuest(player, postingKey) {
    let data = {type: "daily"};
    let tx = await createCustomJSON(player, postingKey, "sm_refresh_quest", data);
    
    await requests.postBroadcastSend(tx)
        .then((x)=>console.log(x))
        .catch(()=> console.log('Failed send broadcast refreshDailyQuest .'));
    
}


async function rankedMatchBotLoop(myCards, quest, player, postingKey) {
    // init match
    let match;

    // Check Outstanding Match
    console.log('check outstanding match')
    let outstanding = await requests.getOutstandingMatch(player)
        .then(x=>x)
        .catch(()=> {console.log('Failed checking outstanding match.'); process.exit(1)});

    if (outstanding == null) {
        // Find New Match
        console.log('no outstanding match, Find new match...')

        match = await findMatch(player, postingKey)
            .then(x => {console.log('Match found! Match ID:', x.id); return x})
            .catch(()=> {console.log('Failed on Find match.'); process.exit(1)});
        
        console.log('wait 5 secs')
        await sleep(5000);
        match = await requests.getBattleStatus(match.id);
    } else {
        console.log('Continuing outstanding match id:', outstanding.id);
        match = await requests.getBattleStatus(outstanding.id);
    }

    // Check if all matchDetails completely returned
    let battleStatusRetries = 0;
    while (!match || match.inactive == null || match.mana_cap == null || match.ruleset == null) {
        battleStatusRetries++;
        if (battleStatusRetries > 20) {
            console.log('No battle status returned, close this session');
            process.exit(1);
        }
        console.log('trying to get complete match details by retrying getBattleStatus')
        await sleep(5000);
        match = await requests.getBattleStatus(match.id);
    }
    
    // Get Mana, Rules and Active Splinters from Match Details
    let splinters = ['Red', 'Blue', 'Black', 'Gold', 'White', 'Green'];
    let inactiveSplinters = match.inactive.split(',');
    let activeSplinters = splinters.filter(i => !inactiveSplinters.includes(i));
    console.log('splinters:', activeSplinters);
    console.log('mana_cap:', match.mana_cap);
    console.log('ruleset:', match.ruleset);

    const matchDetails = {
        mana: match.mana_cap,
        rules: match.ruleset,
        splinters: activeSplinters,
        rating_level: JSON.parse(match.settings).rating_level,
        myCards: myCards
    }

    // Pass the Match Details to Possible Teams script to find best possible team for match details
    const possibleTeams = await ask.possibleTeams(matchDetails).catch(e=>console.log('Error when trying to get Team Candidates: ',e));

    if (possibleTeams && possibleTeams.length) {
        console.log('Team Candidates based on your cards: ', possibleTeams.length);
    } else {
        console.log('Error:', matchDetails, possibleTeams)
        throw new Error('NO TEAMS available to be played');
    }
    
    //Run the algorithm for team selection based on agregated history data against match rulesets
    const teamToPlay = await ask.teamSelection(possibleTeams, matchDetails, quest);
    if (teamToPlay) {
        console.log('Team selected.')
    } else {
        throw new Error('Team Selection error');
    }

    // verbose ? console.log('teamToPlay', teamToPlay) : ''
    console.log('wait 10s')
    await sleep(10000)
    
    // Submit Hashed Team
    console.log('dispatch submitTeam')
    const submit = await submitTeam(match.id, myCards, teamToPlay, player, postingKey)
        .then(x => {console.log('Team submitted! ', x); return true})
        .catch(()=> {console.log('Failed to submit.'); process.exit(1)});
    
    console.log('wait before loop checking')
    await sleep(5000)
    if (submit) {
        
        let wait = 0;
        while (wait < 21) {
            console.log('wait 5s:', wait)
            if (wait == 20) {
                // submit anyway
                await revealTeam(match.id, team, player, postingKey)
                    .then(x => console.log('Team revealed! ', x))
                    .catch(()=> {console.log('Failed to reveal team.'); process.exit(1)});
                
                break;
            }
            await sleep(5000);
            // Check before Reveal Team
            let readyToReveal = await checkBattleStatus(match.id);
            if (readyToReveal) {
                await revealTeam(match.id, team, player, postingKey)
                    .then(x => console.log('Team revealed! ', x))
                    .catch(()=> {console.log('Failed to reveal team.'); process.exit(1)});
                
                break;
                
            } else {
                wait++;
            }
        }
    }
}

// Main Loop
(async () => {
    while (true) {
        console.log('Fetching bot data from DB server')
        credentials = await requests.init()
            .then((x)=>{console.log('data fetched'); return x})
            .catch(()=> {console.log('DB didn\'t respond .'); process.exit(1)});
        if (credentials) {
            running = true;
        }

        while (running) {
            // Start new session and login:
            player = credentials.username;
            _postingKey = credentials.password;
            postingKey = hiveTx.PrivateKey.from(_postingKey)
            
            console.log('START ', player, new Date().toLocaleString())

            // Get initial player details
            details = await requests.getDetails(player)
                .then((x)=>{console.log('fetched player\'s details'); return x})
                .catch(()=> {console.log('get Details api didnt respond.'); process.exit(1)});
            console.log(player, 'Rating:', details ? details.rating : '', 'CP:', details ? details.collection_power : '', 'ECR:', details ? details.capture_rate/100 : 'unknown', '%')

            // Prepare cards
            console.log('getting player\'s cards collection from splinterlands API...')
            myCards = await requests.getPlayableCards(player)
                .then((x)=>{console.log('cards collection ready!'); return x})
                .catch(()=> {console.log('cards collection api didnt respond.'); process.exit(1)});

            // Quest counter and ECR
            let left = 1;
            let ECR = 10000;

            console.log(player, 'getting quest info from splinterlands API...')
            quest = await getQuest(player);
            if(quest) {
                // Avoid Sneak and Snipe quest to have faster completion on daily quests
                const avoid = ['sneak', 'snipe'];

                if (quest.refresh == null && quest.claim == null && avoid.some(v => quest.splinter.includes(v))) {
                    try {
                        
                        console.log(player, 'atempt to get easier quest')
                        
                        await refreshDailyQuest(player, postingKey);
                    } catch (e) {
                        console.log('Fail when atempted to refresh daily quest')
                        process.exit(1)
                    }
                    
                }
                
            } else {
                console.log('Error for quest details.')
            }
    
            
            // The auto battling loop
            while (left || ECR > 7500) {

                try {
                    console.log(player, 'Doing Ranked Match Loop with Rating:', details ? details.rating : '', 'and', ECR ? ECR/100 : '', '% ECR at', new Date().toLocaleString());

                    await rankedMatchBotLoop(myCards, quest, player, postingKey)
                        .then(() => {
                            console.log(player, 'Closing battle', new Date().toLocaleString());        
                        })
                        .catch((e) => {
                            console.log(e, 'Error on RankedMatchBotLoop Catch')
                        })
                    //Check if quest completed 
                    quest = await getQuest(player);
                    left = quest.total - quest.completed;
                    // if (verbose) {
                    //     console.log('Quest details: ', quest);
                    // } else {
                    //     console.log('Quest left:', left, 'Claimed:', quest.claimed)
                    // }
                    console.log('Quest left:', left, 'Claimed:', quest.claim)
                    // claim quest reward when DQ completed
                    if (!left && quest.claim == null) {
                        await claimDailyQuestReward(player, postingKey, quest.id);
                        console.log('daily quest claimed');
                    }
                    //Get Battle Result and Update details
                    try {
                        const result = await requests.getBattleResult(lastReveal)
                        if (result) {
                            console.log(result.winner == player ? player + ' won! Reward ' + JSON.parse(result.dec_info).reward + ' DEC' : 'Battle lost')
                            details = result.player_1_data;
                            if (result.player_1 !== player) {
                                details = result.player_2_data;
                            }
                            ECR = details.capture_rate;
                            requests.update(player, details.rating, details.capture_rate, details.collection_power);
                            console.log(player, 'Rating:', details.rating, 'CP:', details.collection_power, 'ECR:', details.capture_rate);
                        } 
                    } catch (error) {
                        console.log('Fail getting battle result')
                    }
                    
                    
                } catch (e) {
                    console.log('Routine error at: ', new Date().toLocaleString(), e)
                }
                console.log(player,' waiting for the next battle in', sleepingTime/1000, 'seconds' )
                await sleep(sleepingTime);
            }
            
            running = false;
            await requests.idle(player);
            console.log('Account change.')
        }
        
    }
})();