require('dotenv').config()
const quests = require('./quests');
const ask = require('./possibleTeams');
const requests = require('./requests.js');

const hiveTx = require('hive-tx');

function sleep(ms) {
    return new Promise(resolve=>setTimeout(resolve, ms))
}

async function createCustomJSON(player, key, id, data, active = false) {
    let privateKey = hiveTx.PrivateKey.from(key)
    let operations = [[
        "custom_json",
        {
            "required_auths": active ? [player] : [],
            "required_posting_auths": active ? [] : [player],
            "id": id,
            "json": JSON.stringify(data)
        }
    ]];

    const tx = new hiveTx.Transaction()
    await tx.create(operations).then(() => tx.sign(privateKey))

    let encoded = new URLSearchParams();
    encoded.append('signed_tx', JSON.stringify(tx.signedTransaction));

    return encoded.toString();
}

async function harvestSeasonReward(player, activeKey, season_id) {
    let data = { type: 'league_season', season: season_id};
    let tx = await createCustomJSON(player, activeKey, "sm_claim_reward", data, true)
    
    let result = await requests.postBroadcastSend(tx);
    console.log(result);
}


let crops;
let season_id;

(async () => {

    console.log('Start harvesting')
    crops = await requests.initHarvest();
    
    console.log('Crops retrieved');

    const settings = await requests.getSettings()
    season_id = settings.season.id - 1
    console.log('Harvesting Season Reward for season:', season_id, 'begin...')
    // Claiming loop
    for (let i = 0; i < crops.length; i++) {
        console.log('Claiming Season Reward for:', crops[i].username)
        await harvestSeasonReward(crops[i].username, crops[i].key, season_id)
        console.log('Reward claimed')
    }

    console.log('done, all reward harvested')
})();