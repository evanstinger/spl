require('dotenv').config()
const { Telegraf } = require('telegraf')
const token = process.env.TELEGRAM_BOT_TOKEN
const chat_id = process.env.TELEGRAM_OVERLORD_CHAT_ID="365874331"
const bot = new Telegraf(token)
const fetch = require("node-fetch");
const phantomCards = require('./data/phantomCards');
const api_uri = process.env.API_URI;
const api_domain = process.env.API_DOMAIN;

const fetchPlus = (url, options = {}, retries) =>
  fetch(url, options)
    .then(res => {
      if (res.ok) {
        return res.json()
      }
      if (retries > 0) {
        return fetchPlus(url, options, retries - 1)
      }
      throw new Error(res.status)
    })
    .catch(error => console.error(error.message))

async function init () {
    const options = {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({data: {request: 'init'}})
    }
    const content = await fetchPlus(`https://${api_domain}/api/bot/${api_uri}/init`, options, 3)
    return content;
}

async function initHarvest () {
    const options = {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({data: {request: 'initHarvest'}})
    }
    const content = await fetchPlus(`https://${api_domain}/api/bot/${api_uri}/init`, options, 3)
    return content;
}

async function idle (player) {
    const options = {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({data: {request: 'idle', username: player}})
    }
    const content = await fetchPlus(`https://${api_domain}/api/bot/${api_uri}/idle`, options, 3)
    return content;
}

async function update (player, rating, ecr, cp) {
    const options = {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: JSON.stringify({data: {request: 'update', username: player, rating: rating, ecr: ecr, cp: cp}})
    }
    await fetchPlus(`https://${api_domain}/api/bot/${api_uri}/update`, options, 3)
    
}

async function getSettings () {
    try {
        const options = {
            method: 'GET',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            body: null
        }

        const content = await fetchPlus(`https://api.splinterlands.io/settings`, options, 3)
        return content;
    } catch (error) {
        console.log('Failed to get Splinterlands settings')
        return false;
    }
}

async function findCardbyID (id) {
    try {
        const options = {
            method: 'GET',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            body: null
        }
        const content = await fetchPlus(`https://api.splinterlands.io/cards/find?ids=${id}`, options, 3)
        return content;
    } catch (error) {
        console.log('Failed to get Splinterlands settings')
        return false;
    }
}

async function getBattleResult (id) {
    try {
        const options = {
            method: 'GET',
            headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
            },
            body: null
        }
        const content = await fetchPlus(`https://api2.splinterlands.com/battle/result?id=${id}`, options, 3)
        return content;
    } catch (error) {
        console.log('Failed to get battle result')
        return false;
    }
}

async function getECR (username) {
    const options = {
        method: 'GET',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: null
    }
    const content = await fetchPlus(`https://api.splinterlands.io/players/balances?username=${username}`, options, 3)
    
    const ecrObj = content.find(({ token }) => token === 'ECR');
    return parseInt(ecrObj.balance);
}

async function getDECbalance (username) {
    const options = {
        method: 'GET',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: null
    }
    const content = await fetchPlus(`https://api.splinterlands.io/players/balances?username=${username}`, options, 3)
    
    const ecrObj = content.find(({ token }) => token === 'DEC');
    return parseInt(ecrObj.balance);
}

async function getOwnedCards (username) {
    const options = {
        method: 'GET',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: null
    }
    const content = await fetchPlus(`https://game-api.splinterlands.io/cards/collection/${username}`, options, 3)
    const cards = content.cards;
    return cards.filter(x => x.player == username).map(card => card.uid);
}

async function getDetails (username) {
    const options = {
        method: 'GET',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: null
    }
    const content = await fetchPlus(`https://api2.splinterlands.com/players/details?name=${username}`, options, 3)
    return content;
}

async function getOutstandingMatch (username) {
    const options = {
        method: 'GET',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: null
    }
    const content = await fetchPlus(`https://api2.splinterlands.com/players/outstanding_match?username=${username}`, options, 3)
    return content;
}

async function getBattleStatus (id) {
    const options = {
        method: 'GET',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: null
    }
    const content = await fetchPlus(`https://api2.splinterlands.com/battle/status?id=${id}`, options, 3)
    return content;
}

async function postBattleTx (data) {
    const options = {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: data
    }
    const content = await fetchPlus(`https://api2.splinterlands.com/battle/battle_tx`, options, 3)
    return content;
}

async function postBroadcastSend (data) {
    const options = {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: data
    }
    const content = await fetchPlus(`https://broadcast.splinterlands.com/send`, options, 3)
    return content;
}

getPlayerCards = (username) => (fetch(`https://game-api.splinterlands.io/cards/collection/${username}`,
  { "credentials": "omit", "headers": { "accept": "application/json, text/javascript, */*; q=0.01" }, "referrer": `https://splinterlands.com/?p=collection&a=${username}`, "referrerPolicy": "no-referrer-when-downgrade", "body": null, "method": "GET", "mode": "cors" })
  .then(x => x && x.json())
  .then(x => x['cards'] ? x['cards'].filter(x=>x.delegated_to === null || x.delegated_to === username).map(card => ({card_detail_id: card.card_detail_id, uid: card.uid, level: card.level})) : '')
  .then(advanced => phantomCards.concat(advanced))
  .catch(e=> {
    console.log('Error: game-api.splinterlands did not respond trying api.slinterlands... ');
    fetch(`https://api.splinterlands.io/cards/collection/${username}`,
      { "credentials": "omit", "headers": { "accept": "application/json, text/javascript, */*; q=0.01" }, "referrer": `https://splinterlands.com/?p=collection&a=${username}`, "referrerPolicy": "no-referrer-when-downgrade", "body": null, "method": "GET", "mode": "cors" })
      .then(x => x && x.json())
      .then(x => x['cards'] ? x['cards'].filter(x=>x.delegated_to === null || x.delegated_to === username).map(card => ({card_detail_id: card.card_detail_id, uid: card.uid, level: card.level})) : '')
      .then(advanced => phantomCards.concat(advanced))
      .catch(e => {
        console.log('Using only basic cards due to error when getting user collection from splinterlands: ',e); 
        return phantomCards
      })
  })
)

async function getPlayableCards (username) {
    const options = {
        method: 'GET',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: null
    }
    let collection = await fetchPlus(`https://api.splinterlands.io/cards/collection/${username}`, options, 3)
        .then(x => x['cards'] ? x['cards'].filter(x => x.delegated_to === null || x.delegated_to === username).map(card => ({card_detail_id: card.card_detail_id, uid: card.uid, level: card.level})) : '')
        .then(advanced => phantomCards.concat(advanced))
        .catch(async e => {
            console.log('Failed getting Player\'s cards, trying other API');
            await fetchPlus(`https://api.steemmonsters.io/cards/collection/${username}`, options, 3)
            .then(x => x['cards'] ? x['cards'].filter(x => x.delegated_to === null || x.delegated_to === username).map(card => ({card_detail_id: card.card_detail_id, uid: card.uid, level: card.level})) : '')
            .then(advanced => phantomCards.concat(advanced))
            .catch(async e => {
                console.log('Failed getting Player\'s cards, trying other API');
                await fetchPlus(`https://game-api.splinterlands.io/cards/collection/${username}`, options, 3)
                .then(x => x['cards'] ? x['cards'].filter(x => x.delegated_to === null || x.delegated_to === username).map(card => ({card_detail_id: card.card_detail_id, uid: card.uid, level: card.level})) : '')
                .then(advanced => phantomCards.concat(advanced))
                .catch(async e => {
                    console.log('Failed getting cards collection from all available API')
                    await bot.telegram.sendMessage(chat_id, 'Cards Collection API error!')
                    throw new Error();
                })
            })
        })
    return collection;
}

module.exports.init = init;
module.exports.initHarvest = initHarvest;
module.exports.idle = idle;
module.exports.update = update;
module.exports.getSettings = getSettings;
module.exports.findCardbyID = findCardbyID;
module.exports.getBattleResult = getBattleResult;
module.exports.getECR = getECR;
module.exports.getDECbalance = getDECbalance;
module.exports.getOwnedCards = getOwnedCards;
module.exports.getDetails = getDetails;
module.exports.getOutstandingMatch = getOutstandingMatch;
module.exports.getBattleStatus = getBattleStatus;
module.exports.postBattleTx = postBattleTx;
module.exports.postBroadcastSend = postBroadcastSend;
module.exports.getPlayableCards = getPlayableCards;
module.exports.getPlayerCards = getPlayerCards;