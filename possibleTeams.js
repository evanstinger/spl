require('dotenv').config()
const helper = require('./helper');
const battles = require('./battles');
const verbose = process.env.VERBOSE || false;

const summoners = [{ 224: 'dragon' },
{ 27: 'earth' },
{ 16: 'water' },
{ 156: 'life' },
{ 189: 'earth' },
{ 167: 'fire' },
{ 145: 'death' },
{ 5: 'fire' },
{ 71: 'water' },
{ 114: 'dragon' },
{ 178: 'water' },
{ 110: 'fire' },
{ 49: 'death' },
{ 88: 'dragon' },
{ 38: 'life' },
{ 239: 'life' },
{ 74: 'death' },
{ 78: 'dragon' },
{ 260: 'fire' },
{ 70: 'fire' },
{ 109: 'death' },
{ 111: 'water' },
{ 112: 'earth' },
{ 130: 'dragon' },
{ 72: 'earth' },
{ 235: 'dragon' },
{ 56: 'dragon' },
{ 113: 'life' },
{ 200: 'dragon' },
{ 236: 'fire' },
{ 240: 'dragon' },
{ 254: 'water' },
{ 257: 'water' },
{ 258: 'death' },
{ 259: 'earth' },
{ 261: 'life' },
{ 262: 'dragon' },
{ 278: 'earth' },
{ 73: 'life' }]

const splinters = ['fire', 'life', 'earth', 'water', 'death', 'dragon'];
const colorToDeck = [
    [ 'Red', 'fire' ],
    [ 'Blue', 'water' ],
    [ 'White', 'life' ],
    [ 'Black', 'death' ],
    [ 'Green', 'earth' ],
    [ 'Gold', 'dragon' ]
];

const getSplinters = (colors) => {
    const res = colorToDeck.filter(([key, value]) => colors.includes(key)).map(([key, value]) => value);
    return res;
}


const getSummoners = (myCards) => {
    try {
        const sumArray = summoners.map(x=>Number(Object.keys(x)[0]))
        const mySummoners = myCards.filter(value => sumArray.includes(Number(value)));
        return mySummoners;             
    } catch(e) {
        console.log(e);
        return [];
    }
}

const summonerColor = (id) => {
    const summonerDetails = summoners.find(x => x[id]);
    return summonerDetails ? summonerDetails[id] : '';
}

const historyBackup = require("./data/newHistory.json");
const basicCards = require('./data/phantomCards.js');

let availabilityCheck = (base, toCheck) => toCheck.slice(0, 7).every(v => base.includes(v));

const battlesFilterByManacap = async (mana, ruleset, summoners) => {
    
    return historyBackup.filter(
        battle =>
            battle.mana_cap == mana &&
            summoners.includes(battle.summoner_id) &&
            (ruleset ? battle.ruleset === ruleset : true)
    )
}

function compare(a, b) {
    const totA = a[9];
    const totB = b[9];
  
    let comparison = 0;
    if (totA < totB) {
      comparison = 1;
    } else if (totA > totB) {
      comparison = -1;
    }
    return comparison;
  }

const cardsIdsforSelectedBattles = (mana, ruleset, splinters, summoners) => battlesFilterByManacap(mana, ruleset, summoners)
    .then(x => {
        return x.map(
            (x) => {
                return [
                    x.summoner_id ? parseInt(x.summoner_id) : '',
                    x.monster_1_id ? parseInt(x.monster_1_id) : '',
                    x.monster_2_id ? parseInt(x.monster_2_id) : '',
                    x.monster_3_id ? parseInt(x.monster_3_id) : '',
                    x.monster_4_id ? parseInt(x.monster_4_id) : '',
                    x.monster_5_id ? parseInt(x.monster_5_id) : '',
                    x.monster_6_id ? parseInt(x.monster_6_id) : '',
                    summonerColor(x.summoner_id) ? summonerColor(x.summoner_id) : '',
                    x.tot ? parseInt(x.tot) : '',
                    x.ratio ? parseInt(x.ratio) : '',
                ]
            }
        ).filter(
            team => splinters.includes(team[7])
        ).sort(compare)
    })

const askFormation = function (matchDetails) {
    const cards = matchDetails.myCards || basicCards;
    const cardsIds = cards.map(x => x.card_detail_id);
    cardsIds.push(''); //add empty card
    const mySummoners = getSummoners(cardsIds);
    const splinters = getSplinters(matchDetails.splinters);
    // verbose ? console.log('Algo inputs: ', matchDetails.mana, matchDetails.rules, splinters, mySummoners, cards.length) : '';
    return cardsIdsforSelectedBattles(matchDetails.mana, matchDetails.rules, splinters, mySummoners)
        .then(x => x.filter(
            x => availabilityCheck(cardsIds, x))
            .map(element => element)
        )

}

const possibleTeams = async (matchDetails) => {
    let possibleTeams = [];
    while (matchDetails.mana > 10) {
        console.log('check battles based on mana: '+matchDetails.mana)
        possibleTeams = await askFormation(matchDetails)
        if (possibleTeams.length > 0) {
            return possibleTeams;
        }
        matchDetails.mana--;
    }
    return possibleTeams;
}

const mostWinningSummonerTankCombo = async (possibleTeams, matchDetails) => {
    const bestCombination = await battles.mostWinningSummonerTank(possibleTeams)
    // verbose ? console.log('BEST SUMMONER and TANK', bestCombination) : '';
    if (bestCombination.summonerWins >= 1 && bestCombination.tankWins > 1 && bestCombination.backlineWins > 1 && bestCombination.secondBacklineWins > 1 && bestCombination.thirdBacklineWins > 1 && bestCombination.forthBacklineWins > 1) {
        const bestTeam = await possibleTeams.find(x => x[0] == bestCombination.bestSummoner && x[1] == bestCombination.bestTank && x[2] == bestCombination.bestBackline && x[3] == bestCombination.bestSecondBackline && x[4] == bestCombination.bestThirdBackline && x[5] == bestCombination.bestForthBackline)
        // console.log('BEST TEAM', bestTeam)
        const summoner = bestTeam[0].toString();
        return [summoner, bestTeam];
    }
    if (bestCombination.summonerWins >= 1 && bestCombination.tankWins > 1 && bestCombination.backlineWins > 1 && bestCombination.secondBacklineWins > 1 && bestCombination.thirdBacklineWins > 1) {
        const bestTeam = await possibleTeams.find(x => x[0] == bestCombination.bestSummoner && x[1] == bestCombination.bestTank && x[2] == bestCombination.bestBackline && x[3] == bestCombination.bestSecondBackline && x[4] == bestCombination.bestThirdBackline)
        // verbose ? console.log('BEST TEAM', bestTeam) : '';
        const summoner = bestTeam[0].toString();
        return [summoner, bestTeam];
    }
    if (bestCombination.summonerWins >= 1 && bestCombination.tankWins > 1 && bestCombination.backlineWins > 1 && bestCombination.secondBacklineWins > 1) {
        const bestTeam = await possibleTeams.find(x => x[0] == bestCombination.bestSummoner && x[1] == bestCombination.bestTank && x[2] == bestCombination.bestBackline && x[3] == bestCombination.bestSecondBackline)
        // verbose ? console.log('BEST TEAM', bestTeam) : '';
        const summoner = bestTeam[0].toString();
        return [summoner, bestTeam];
    }
    if (bestCombination.summonerWins >= 1 && bestCombination.tankWins > 1 && bestCombination.backlineWins > 1) {
        const bestTeam = await possibleTeams.find(x => x[0] == bestCombination.bestSummoner && x[1] == bestCombination.bestTank && x[2] == bestCombination.bestBackline)
        // verbose ? console.log('BEST TEAM', bestTeam) : '';
        const summoner = bestTeam[0].toString();
        return [summoner, bestTeam];
    }
    if (bestCombination.summonerWins >= 1 && bestCombination.tankWins > 1) {
        const bestTeam = await possibleTeams.find(x => x[0] == bestCombination.bestSummoner && x[1] == bestCombination.bestTank)
        // verbose ? console.log('BEST TEAM', bestTeam) : '';
        const summoner = bestTeam[0].toString();
        return [summoner, bestTeam];
    }
    if (bestCombination.summonerWins >= 1) {
        const bestTeam = await possibleTeams.find(x => x[0] == bestCombination.bestSummoner)
        // verbose ? console.log('BEST TEAM', bestTeam) : '';
        const summoner = bestTeam[0].toString();
        return [summoner, bestTeam];
    }
}

const teamSelection = async (possibleTeams, matchDetails, quest) => {
    let priorityToTheQuest = process.env.QUEST_PRIORITY === 'false' ? false : true;
    //TEST V2 Strategy ONLY FOR PRIVATE API
    // if (process.env.API_VERSION == 2 && possibleTeams[0][8]) {
    //     //check quest for private api V2:
    //     if(priorityToTheQuest && possibleTeams.length > 25 && quest && quest.total) {
    //         const left = quest.total - quest.completed;
    //         const questCheck = matchDetails.splinters.includes(quest.splinter) && left > 0;
    //         const filteredTeams = possibleTeams.filter(team=>team[7]===quest.splinter)
    //         console.log(left + ' battles left for the '+quest.splinter+' quest')
    //         console.log('play for the quest ',quest.splinter,'? ',questCheck)
    //         if(left > 0 && filteredTeams && filteredTeams.length > 1 && splinters.includes(quest.splinter) && filteredTeams[0][8]) {
    //             console.log('PLAY for the quest with Teams: ',filteredTeams.length, filteredTeams, 'PLAY: ', filteredTeams[0])
    //             return { summoner: filteredTeams[0][0], cards: filteredTeams[0] };
    //         }
    //     }
    //     console.log('play the most winning: ', possibleTeams[0])
    //     return { summoner: possibleTeams[0][0], cards: possibleTeams[0] };
    // }
    

    //check if daily quest is not completed
    console.log('quest custom option set as:', process.env.QUEST_PRIORITY)
    if(priorityToTheQuest && possibleTeams.length > 25 && quest && quest.total) {
        const left = quest.total - quest.completed;
        const questCheck = matchDetails.splinters.includes(quest.splinter) && left > 0;
        const filteredTeams = possibleTeams.filter(team=>team[7]===quest.splinter)
        console.log(left + ' battles left for the '+quest.splinter+' quest')
        console.log('play for the quest ',quest.splinter,'? ',questCheck)
        if(left > 0 && filteredTeams && filteredTeams.length > 3 && splinters.includes(quest.splinter)) {
            console.log('Available teams to play with this quest: ',filteredTeams.length)
            const res = await mostWinningSummonerTankCombo(filteredTeams, matchDetails);
            console.log('Play this team for the quest.')
            if (res[0] && res[1]) {
                return { summoner: res[0], cards: res[1] };
            }
        }
    }

    //find best combination (most used)
    const res = await mostWinningSummonerTankCombo(possibleTeams, matchDetails);
    console.log('Dont play for the quest, and play this')
    if (res[0] && res[1]) {
        return { summoner: res[0], cards: res[1] };
    }

    let i = 0;
    const makeCardId = (id) => id;
    for (i = 0; i <= possibleTeams.length - 1; i++) {
        if (matchDetails.splinters.includes(possibleTeams[i][7]) && helper.teamActualSplinterToPlay(possibleTeams[i]) !== '' && matchDetails.splinters.includes(helper.teamActualSplinterToPlay(possibleTeams[i]).toLowerCase())) {
            console.log('Less than 25 teams available. SELECTED: ', possibleTeams[i]);
            const summoner = makeCardId(possibleTeams[i][0].toString());
            return { summoner: summoner, cards: possibleTeams[i] };
        }
        console.log('DISCARDED: ', possibleTeams[i])
    }
    throw new Error('NO TEAM available to be played.');
}


module.exports.possibleTeams = possibleTeams;
module.exports.teamSelection = teamSelection;